FROM centos:6

MAINTAINER Yuji Yamamoto <whosekiteneverfly@gmail.com>

RUN yum -y update

RUN curl -sSL https://s3.amazonaws.com/download.fpcomplete.com/centos/6/fpco.repo | tee /etc/yum.repos.d/fpco.repo && \
    yum install -y zlib-devel stack

# ENTRYPOINT echo DO NOT OVERWRITE ENTRYPOINT
