{-# LANGUAGE OverloadedStrings #-}

import           Control.Monad (when, unless)
import           Data.Char (isDigit)
import           System.Environment (getArgs)
import           System.Exit (exitFailure)
import           Web.Spock

main :: IO ()
main = do
    args <- getArgs

    when (null args) $ do
        putStrLn "Specify port number"
        exitFailure

    let portStr = Prelude.head args
    unless (all isDigit portStr) $ do
        putStrLn "Specify port number"
        exitFailure

    let port = read $ portStr
    runSpock port $ spockT id $ do
      get root $ html "<h1>Hello, stack docker integration!</h1>\n"
