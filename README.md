# stack-docker-sample

[StackのDocker integration](https://docs.haskellstack.org/en/stable/docker_integration/)のサンプルです。  
実際の発表はこれ（英語）: [Introduction to Stack's Docker Integration (31)](http://the.igreque.info/slides/2016-09-17-stack-docker-integration.html)

- 依存するソフトウェア:
    - [haskell-stack](https://docs.haskellstack.org/en/stable/README/)
    - [docker](https://www.docker.com/products/docker-engine)
- 実行方法: `stack exec stack-docker-sample -- <port_number>`
- ビルド方法:
    - `docker build -t stack_docker_sample_server:latest .`
    - `stack setup`
    - `stack build`

## Dockerなしでビルド・実行する場合

[haskell-stackのみをインストール](https://docs.haskellstack.org/en/stable/README/#how-to-install)した上で

```bash
stack --no-docker build
```

すると、ビルドできます。  
GHCがインストールされていない場合は、初回は`stack --no-docker setup`が必要かもしれません。

ビルドが終わったら

```bash
stack --no-docker exec stack-docker-sample -- 3000
```

で実行してください。
